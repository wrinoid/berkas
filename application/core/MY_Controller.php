<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('M_system');
	}

	public function prepare_data($title = "", $menu, $anak_menu){
		$data['title'] = $title;
		$data['menu'] = $menu;
		$data['anak_menu'] = $anak_menu;
		$data['script_top'] = "";
		$data['script_bottom'] = "";

		return $data;
	}

	public function authentication(){
		if($this->input->cookie('login', TRUE)){

			$user = $this->M_system->get('t_users', ['user_id' => $this->input->cookie('id')])->row();

			if($user){
				$exp = '';
				$remember = FALSE;
				if($this->input->cookie('remember', TRUE)){
					$exp = '2592000';
					$remember = TRUE;
				}
				else{
					$exp = '21600';
				}

				$cookie = [
					'name'   => 'remember',
					'value'  => $remember,
					'expire' => $exp,
				];
				$this->input->set_cookie($cookie);
				$cookie = [
					'name'   => 'login',
					'value'  => TRUE,
					'expire' => $exp,
				];
				$this->input->set_cookie($cookie);
				$cookie = [
					'name'   => 'username',
					'value'  => $user->username,
					'expire' => $exp,
				];
				$this->input->set_cookie($cookie);
				$cookie = [
					'name'   => 'name',
					'value'  => $user->name,
					'expire' => $exp,
				];
				$this->input->set_cookie($cookie);
				$cookie = [
					'name'   => 'id',
					'value'  => $user->user_id,
					'expire' => $exp,
				];
				$this->input->set_cookie($cookie);

				if($user->gdrive_token == NULL){
					redirect('oauth2');
				}

				return TRUE;
			}
			else{
				return FALSE;
			}
		}
		else{
			return FALSE;
		}
	}

}
