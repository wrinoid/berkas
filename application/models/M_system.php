<?php
class M_system extends CI_Model {

	private $db;

	public function __construct(){
		parent::__construct();
		$this->db = $this->load->database('system', TRUE);
	}

	/* GET DATA */
	public function get($table, $where){
		$query = $this->db->get_where($table, $where);
		return $query;
	}
	
	// /* SELECT WHERE LIMITED */
	// public function getAllDataLimited($table, $column_id, $limit, $offset, $where = array()){
	// 	$this->db->order_by($column_id, 'DESC');
	// 	$query = $this->db->get_where($table, $where, $limit, $offset);
	// 	return $query;
	// }

	/* INSERT */
	public function insert($table, $data){
		$this->db->insert($table, $data);
		$insert_id = $this->db->insert_id();

		return  $insert_id;
	}
	
	/* INSERT BATCH */
	public function insert_batch($table, $data){
		$this->db->insert_batch($table, $data);
	}
	
	/* UPDATE */
	public function update($table, $where, $data){
		$this->db->where($where);
		$this->db->update($table, $data);
	}
	
	/* DELETE */
	public function remove($table, $where){
		$this->db->where($where);
		$this->db->delete($table);
	}	
}
?>