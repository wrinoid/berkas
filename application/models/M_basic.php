<?php
class M_basic extends CI_Model {

	private $sql;

	public function __construct(){
		parent::__construct();
		$this->sql = $this->db;
	}

	/* FILTER SOFT DELETE */
	private function with_deleted($table, $param){
		$column_exist = $this->db->query('SHOW COLUMNS FROM '.$table.' LIKE "is_delete"')->num_rows();

		$available_type = ['default', 'deleted', 'all'];
		$param = (in_array($param, $available_type)) ? $param : 'default';

		if($param == "default"){ // without deleted record
			if($column_exist > 0) $this->sql->where('is_delete', 1);
		}
		elseif($param == "deleted"){ // only deleted record
			if($column_exist > 0) $this->sql->where('is_delete', 2);
		}
	}

	/* GET DATA */
	public function get($table, $where = [], $with_deleted = "default"){
		$this->with_deleted($table, $with_deleted);
		$query = $this->sql->get_where($table, $where);

		return $query;
	}

	/* COUNT DATA */
	public function count($table, $where = "", $with_deleted = "default"){
		$this->with_deleted($table, $with_deleted);
		if($where != ""){
			$this->sql->where($where);
		}
		$this->sql->from($table);
		$query = $this->sql->count_all_results();

		return $query;
	}
	
	/* INSERT */
	public function insert($table, $data){
		$this->db->insert($table, $data);
		$insert_id = $this->db->insert_id();

		return $insert_id;
	}
	
	/* INSERT BATCH */
	public function insert_batch($table, $data){
		$this->db->insert_batch($table, $data);
	}
	
	/* UPDATE */
	public function update($table, $where, $data){
		$this->db->where($where);
		$this->db->update($table, $data);
	}
	
	/* DELETE */
	public function remove($table, $where){
		$this->db->where($where);
		$this->db->delete($table);
	}	
}
?>