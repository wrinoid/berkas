<?php
class M_file extends CI_Model {

	private $sql;

	public function __construct(){
		parent::__construct();
		$this->sql = $this->db;
	}

	/* FILTER SOFT DELETE */
	private function with_deleted($table, $param){
		$column_exist = $this->db->query('SHOW COLUMNS FROM '.$table.' LIKE "is_delete"')->num_rows();

		$available_type = ['default', 'deleted', 'all'];
		$param = (in_array($param, $available_type)) ? $param : 'default';

		if($param == "default"){ // without deleted record
			if($column_exist > 0) $this->sql->where('is_delete', 1);
		}
		elseif($param == "deleted"){ // only deleted record
			if($column_exist > 0) $this->sql->where('is_delete', 2);
		}
	}

	/* GET DATA */
	public function get_file_datatable($limit, $start, $order, $dir, $where = "", $with_deleted = "default"){
		$this->with_deleted('t_files', $with_deleted);
		if($where != ""){
			$this->sql->where($where);
		}
		$this->sql->from('t_files');
		$this->sql->limit($limit, $start);
		$this->sql->order_by($order, $dir);
		$query = $this->sql->get();

		return $query->result();
	}
}
?>