<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('M_system');
	}

	public function login(){
		if($this->authentication()) redirect('welcome');

		if($this->input->post('submit')){

			$user = $this->M_system->get('t_users', ['username' => $this->input->post('username')])->row();

			if($user){
				if(password_verify($this->input->post('password'), $user->password)){
					$exp = '';
					$remember = FALSE;
					if($this->input->post('remember')){
						$exp = '2592000';
						$remember = TRUE;
					}
					else{
						$exp = '21600';
					}

					$cookie = [
						'name'   => 'remember',
						'value'  => $remember,
						'expire' => $exp,
					];
					$this->input->set_cookie($cookie);
					$cookie = [
						'name'   => 'login',
						'value'  => TRUE,
						'expire' => $exp,
					];
					$this->input->set_cookie($cookie);
					$cookie = [
						'name'   => 'username',
						'value'  => $user->username,
						'expire' => $exp,
					];
					$this->input->set_cookie($cookie);
					$cookie = [
						'name'   => 'name',
						'value'  => $user->name,
						'expire' => $exp,
					];
					$this->input->set_cookie($cookie);
					$cookie = [
						'name'   => 'id',
						'value'  => $user->user_id,
						'expire' => $exp,
					];
					$this->input->set_cookie($cookie);
					
					redirect('welcome');
				}
				else{
					$this->session->set_flashdata('login', '<div class="callout callout-danger"><p>User and Password didn\'t match.</p></div>');
				}
			}
		}

		$this->load->view('auth/login');
	}

	public function logout(){
		$cookie = [
			'name'   => 'remember',
			'value'  => $remember,
			'expire' => $exp,
		];
		$this->input->set_cookie($cookie);
		$cookie = [
			'name'   => 'login',
			'value'  => FALSE,
			'expire' => 0,
		];
		$this->input->set_cookie($cookie);
		$cookie = [
			'name'   => 'username',
			'value'  => '',
			'expire' => 0,
		];
		$this->input->set_cookie($cookie);
		$cookie = [
			'name'   => 'name',
			'value'  => '',
			'expire' => 0,
		];
		$this->input->set_cookie($cookie);
		$cookie = [
			'name'   => 'id',
			'value'  => '',
			'expire' => 0,
		];
		$this->input->set_cookie($cookie);

		redirect('auth/login');
	}

}
