<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'vendor/autoload.php';

class File extends MY_Controller {

	public function __construct(){
		parent::__construct();

		if(!$this->authentication()) redirect('auth/login');

		$this->load->model('M_basic');
		$this->load->model('M_file');
	}

	public function index()
	{
		$data = $this->prepare_data("List Berkas", "berkas", "list");

		$data['script_top'] = "
		<link rel='stylesheet' type='text/css' href='https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css'>
		<link rel='stylesheet' type='text/css' href='https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css'>
		<style>
			.vertical-middle {
				vertical-align: middle !important;
			}
		</style>
		";

		$data['script_bottom'] = "
		<script type='text/javascript' charset='utf8' src='https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js'></script>
		<script type='text/javascript' charset='utf8' src='https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js'></script>
		<script type='text/javascript' charset='utf8' src='".base_url('assets/plugins/datatables/extensions/Input/input.js')."'></script>
		<script type='text/javascript' charset='utf8' src='https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js'></script>
		<script type='text/javascript' charset='utf8' src='https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js'></script>
		<script type='text/javascript'>
			var table;
			$(document).ready(function(){
				table = $('#table-files').DataTable({
					'processing': true,
					'serverSide': true,
					'responsive': {
						'details': {
							'type': 'column'
						}
					},
					'ajax': {
						'url': '".site_url('file/ajax')."',
						'dataType': 'json',
						'type': 'POST',
						'data': function(d){
							d.type = 'get-list-berkas',
							d.file_type = 'default',
							d.csrf_test_name = $('#csrf').val()
						}
					},
					'columns': [
						{
							'data': 'name',
							'className': 'vertical-middle'
						},
						{
							'data': 'number',
							'className': 'vertical-middle text-center'
						},
						{
							'data': 'date',
							'className': 'vertical-middle text-center'
						},
						{
							'data': 'action',
							'className': 'vertical-middle text-center',
							'orderable': false
						}
					],
					'order': [ 2, 'desc' ],
					'pagingType': 'input',
					'drawCallback': function(settings){
						$('.csrf').val(settings.json.new_hash);
					}
				});

				table_deleted = $('#table-files-deleted').DataTable({
					'processing': true,
					'serverSide': true,
					'responsive': {
						'details': {
							'type': 'column'
						}
					},
					'ajax': {
						'url': '".site_url('file/ajax')."',
						'dataType': 'json',
						'type': 'POST',
						'data': function(d){
							d.type = 'get-list-berkas',
							d.file_type = 'deleted',
							d.csrf_test_name = $('#csrf').val()
						}
					},
					'columns': [
						{
							'data': 'name',
							'className': 'vertical-middle'
						},
						{
							'data': 'number',
							'className': 'vertical-middle text-center'
						},
						{
							'data': 'date',
							'className': 'vertical-middle text-center'
						},
						{
							'data': 'action',
							'className': 'vertical-middle text-center',
							'orderable': false
						}
					],
					'order': [ 2, 'desc' ],
					'pagingType': 'input',
					'drawCallback': function(settings){
						$('.csrf').val(settings.json.new_hash);
					}
				});

				$('a[data-toggle=\"tab\"]').on('shown.bs.tab', function(e){
					if(e.target.text == 'Active'){
						table.columns.adjust();
					}
					else{
						table_deleted.columns.adjust();
					}
				});
			});

			$(document).on('click', '.btn-delete', function(e){
				e.preventDefault();

				var id = $(this).data('id');

				$('#modal-delete').find('.konfirm-delete').data('id', id);
				$('#modal-delete').find('.konfirm-delete').prop('disabled', false);

				$('#modal-delete').modal('show');
			});

			$(document).on('click', '.konfirm-delete', function(e){
				e.preventDefault();

				$('#modal-delete').find('.konfirm-delete').prop('disabled', true);

				var id = $(this).data('id');
				var type = $(this).data('type');
				var csrf = $('#csrf').val();

				$.ajax({
					type: 'POST',
					dataType: 'json',
					url: '".site_url('file/ajax')."',
					data: {'csrf_test_name': csrf, 'id': id, 'type': 'delete-berkas', 'permanent': type},
					success: function(res){
						console.log(res);
						$('.csrf').val(res.new_hash);
						$('.content .row .col-md-12').prepend('<div class=\"alert alert-success alert-dismissible\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button><h4><i class=\"icon fa fa-check\"></i> Sukses!</h4><b> '+ res.message +'</b></div>');
						$('#modal-delete').modal('hide');
						table.ajax.reload();
					},
					error: function(datas){
						console.log(datas);
					}
				});
			});
		</script>
		";

		$this->load->view('static/header', $data);
		$this->load->view('static/menu');
		$this->load->view('file/list');
		$this->load->view('static/footer');
	}

	public function add(){
		if($this->input->post()){
			$this->form_validation->set_rules('name', 'Nama', 'required|max_length[255]');
			$this->form_validation->set_rules('importance_level', 'Tingkat', 'required|in_list[Normal,Penting]');
			$this->form_validation->set_rules('hardfile', 'Fisik', 'required|in_list[Ada,Tidak Ada]');

			if($this->form_validation->run()) {
				$_FILES['userfile'] = $_FILES['file'];

				$config['upload_path']		= './temp/';
				$config['file_name']		= $this->input->post('name');
				$config['allowed_types']	= 'jpg|png|pdf|doc|docx|xls|xlsx|rar';
				$config['max_size']			= 0;

				$this->load->library('upload', $config);

				if(!$this->upload->do_upload('userfile')){
					// fail upload
					$error = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('file_error', $error);
				}
				else{
					// success upload
					$data = $this->upload->data();

					// get user google token
					$user = $this->M_system->get('t_users', ['user_id' => $this->input->cookie('id')])->row();

					if($user->gdrive_token){
						$client = new Google_Client();
						$client->setAccessToken($user->gdrive_token);

						// renew token if expired
						if($client->isAccessTokenExpired()){
							$client->setAuthConfigFile(FCPATH.'client_secret.json');
							$client->fetchAccessTokenWithRefreshToken($user->gdrive_refresh_token);
							$client->setAccessToken(json_encode($client->getAccessToken()));
							
							// update new token to database
							$this->M_system->update('t_users', ['user_id' => $this->input->cookie('id')], ['gdrive_token' => json_encode($client->getAccessToken())]);
						}

						$drive = new Google_Service_Drive($client);

						// get folder wBerkas
						$folder = $drive->files->listFiles([
							'q' =>  "mimeType = 'application/vnd.google-apps.folder' and name = 'wBerkas'"
						]);

						// set metadata to folder wBerkas
						$fileMetadata = new Google_Service_Drive_DriveFile([
							'name' => date("Ymd")."-".$data['file_name'],
							'parents' => array($folder->files[0]->id)
						]);

						// prepare file
						$content = file_get_contents('./temp/'.$data['file_name']);

						// upload file to google drive
						$file = $drive->files->create($fileMetadata, [
							'data' => $content,
							'mimeType' => $data['file_type'],
							'uploadType' => 'multipart',
							'fields' => 'id'
						]);
						
						// get the id of uploaded file
						$gid = $file->id;

						// start mysql transaction
						$this->db->trans_start();

						$i = 0;
						foreach($this->input->post('tags') as $tag){
							$this->db->query('INSERT IGNORE INTO t_tags (name) VALUES ("'.$tag.'")');
							$i++;
						}

						if($i == 0) $tags = ['Uncategorized'];
						else $tags = $this->input->post('tags');

						$insert = [
							'name'					=> $this->input->post('name'),
							'number'				=> $this->input->post('number'),
							'date'					=> $this->input->post('date'),
							'hardfile'				=> $this->input->post('hardfile'),
							'importance_level'		=> $this->input->post('importance_level'),
							'acceptance_date'		=> $this->input->post('acceptance_date'),
							'tags'					=> implode(",", $tags),
							'path'					=> $gid
						];

						$this->M_basic->insert('t_files', $insert);

						// end mysql transaction
						$this->db->trans_complete();

						delete_files('./temp/');

						if($this->db->trans_status() === FALSE){
							// error
							$this->session->set_flashdata('error', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i> Error!</h4><b> Berkas gagal ditambahkan.</b></div>');
						}
						else{
							// success
							$this->session->set_flashdata('success', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i> Sukses!</h4><b> Berkas sukses ditambahkan.</b></div>');
							redirect('file/add');
						}
					}
				}
			}
		}

		$data = $this->prepare_data("Tambah Berkas", "berkas", "add");

		$data['script_top'] = "
		<!-- Select2 -->
		<link rel='stylesheet' href='".base_url('assets/plugins/select2/select2.min.css')."'>
		<!-- Datepicker -->
		<link rel='stylesheet' type='text/css' href='".base_url('assets/plugins/datepicker/datepicker3.css')."'>
		";

		$data['script_bottom'] = "
		<!-- Select2 -->
		<script src='".base_url('assets/plugins/select2/select2.full.min.js')."'></script>
		<!-- Datepicker -->
		<script type='text/javascript' src='".base_url('assets/plugins/datepicker/bootstrap-datepicker.js')."'></script>
		<script type='text/javascript'>
			$(document).ready(function(){
				$('input[name=\"date\"], input[name=\"acceptance_date\"]').datepicker({
					autoclose: true,
					format: 'yyyy-mm-dd',
					todayHighlight: true
				});

				$('select[name=\"tags[]\"]').select2({
					tags: true,
					createTag: function(params){
						return{
							id: params.term,
							text: params.term,
							newOption: true
						}
					},
					templateResult: function(data){
						var result = $('<span></span>');
						result.text(data.text);
						if(data.newOption){
							result.append(' <em>(tambah baru)</em>');
						}
						return result;
					}
				});
			});
		</script>
		";

		$data['tags'] = $this->M_basic->get('t_tags', ['name !=' => 'Uncategorized'])->result();

		$this->load->view('static/header', $data);
		$this->load->view('static/menu');
		$this->load->view('file/add');
		$this->load->view('static/footer');
	}

	public function edit($id = null){
		if($id == null) redirect('file');

		if($this->input->post()){
			$this->form_validation->set_rules('name', 'Nama', 'required|max_length[255]');
			$this->form_validation->set_rules('importance_level', 'Tingkat', 'required|in_list[Normal,Penting]');
			$this->form_validation->set_rules('hardfile', 'Fisik', 'required|in_list[Ada,Tidak Ada]');

			if($this->form_validation->run()) {
				// start mysql transaction
				$this->db->trans_start();

				$i = 0;
				foreach($this->input->post('tags') as $tag){
					$this->db->query('INSERT IGNORE INTO t_tags (name) VALUES ("'.$tag.'")');
					$i++;
				}

				if($i == 0) $tags = ['Uncategorized'];
				else $tags = $this->input->post('tags');

				$update = [
					'name'					=> $this->input->post('name'),
					'number'				=> $this->input->post('number'),
					'date'					=> $this->input->post('date'),
					'hardfile'				=> $this->input->post('hardfile'),
					'importance_level'		=> $this->input->post('importance_level'),
					'acceptance_date'		=> $this->input->post('acceptance_date'),
					'tags'					=> implode(",", $tags)
				];

				$this->M_basic->update('t_files', ['file_id' => $id], $update);

				// end mysql transaction
				$this->db->trans_complete();

				if($this->db->trans_status() === FALSE){
					// error
					$this->session->set_flashdata('error', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i> Error!</h4><b> Berkas gagal diubah.</b></div>');
				}
				else{
					// success
					$this->session->set_flashdata('success', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-check"></i> Sukses!</h4><b> Berkas sukses diubah.</b></div>');
					redirect('file/edit/'.$id);
				}
			}
		}

		$data = $this->prepare_data("Edit Berkas", "berkas", "list");

		$data['script_top'] = "
		<!-- Select2 -->
		<link rel='stylesheet' href='".base_url('assets/plugins/select2/select2.min.css')."'>
		<!-- Datepicker -->
		<link rel='stylesheet' type='text/css' href='".base_url('assets/plugins/datepicker/datepicker3.css')."'>
		";

		$data['script_bottom'] = "
		<!-- Select2 -->
		<script src='".base_url('assets/plugins/select2/select2.full.min.js')."'></script>
		<!-- Datepicker -->
		<script type='text/javascript' src='".base_url('assets/plugins/datepicker/bootstrap-datepicker.js')."'></script>
		<script type='text/javascript'>
			$(document).ready(function(){
				$('input[name=\"date\"], input[name=\"acceptance_date\"]').datepicker({
					autoclose: true,
					format: 'yyyy-mm-dd',
					todayHighlight: true
				});

				$('select[name=\"tags[]\"]').select2({
					tags: true,
					createTag: function(params){
						return{
							id: params.term,
							text: params.term,
							newOption: true
						}
					},
					templateResult: function(data){
						var result = $('<span></span>');
						result.text(data.text);
						if(data.newOption){
							result.append(' <em>(tambah baru)</em>');
						}
						return result;
					}
				});
			});
		</script>
		";

		$data['tags'] = $this->M_basic->get('t_tags', ['name !=' => 'Uncategorized'])->result();
		$data['file'] = $this->M_basic->get('t_files', ['file_id' => $id], 'all')->row();

		$this->load->view('static/header', $data);
		$this->load->view('static/menu');
		$this->load->view('file/edit');
		$this->load->view('static/footer');
	}

	public function ajax(){
		if($this->input->post('type')){
			$type = $this->input->post('type');
			
			if($type == 'get-list-berkas'){
				$file_type = $this->input->post('file_type');

				$columns = array(
					0 => 'name', 
					1 => 'number',
					2 => 'date',
					3 => 'action',
				);
				
				$totalData = $this->M_basic->count('t_files', [], $file_type);

				$totalFiltered = $totalData; 

				$limit = $this->input->post('length');
				$start = $this->input->post('start');
				$order = $columns[$this->input->post('order')[0]['column']];
				$dir = $this->input->post('order')[0]['dir'];

				if(empty($this->input->post('search')['value'])){
					$rows = $this->M_file->get_file_datatable($limit, $start, $order, $dir, [], $file_type);
				}
				else {
					$search = $this->input->post('search')['value'];

					$where = "name like '%".$search."%' OR number like '%".$search."%' OR date like '%".$search."%'";

					$rows = $this->M_file->get_file_datatable($limit, $start, $order, $dir, $where, $file_type);

					$totalFiltered = $this->M_basic->count('t_files', $where, $file_type);
				}

				$data = array();
				foreach($rows as $row){
					$nestedData['name'] = $row->name;
					$nestedData['number'] = $row->number;
					$nestedData['date'] = ($row->date != NULL AND $row->date != '0000-00-00') ? date_format(date_create($row->date), 'd F Y') : '';

					if($file_type == 'default'){
						$nestedData['action'] = "<a href='".site_url('file/download/'.$row->path)."' target='_blank' class='btn-sm btn-success' data-toggle='tooltip' data-title='Download' data-placement='top'><i class='fa fa-cloud-download'></i></a> <a href='".site_url('file/edit/'.$row->file_id)."' class='btn-sm btn-primary' data-toggle='tooltip' data-title='Edit' data-placement='top'><i class='fa fa-edit'></i></a> <a href='#' class='btn-sm btn-danger btn-delete' data-id='".$row->file_id."' data-toggle='tooltip' data-title='Delete' data-placement='top'><i class='fa fa-trash'></i></a>";
					}
					elseif($file_type == 'deleted'){
						$nestedData['action'] = "<a href='#' class='btn-sm btn-success btn-restore' data-id='".$row->file_id."' data-toggle='tooltip' data-title='Restore' data-placement='top'><i class='fa fa-recycle'></i></a> <a href='".site_url('file/edit/'.$row->file_id)."' class='btn-sm btn-primary' data-toggle='tooltip' data-title='Edit' data-placement='top'><i class='fa fa-edit'></i></a> <a href='#' class='btn-sm btn-danger btn-delete-permanent' data-id='".$row->file_id."' data-toggle='tooltip' data-title='Delete Permanent' data-placement='top'><i class='fa fa-remove'></i></a>";
					}

					$data[] = $nestedData;
				}

				$json_data = array(
					"draw"            => intval($this->input->post('draw')),
					"recordsTotal"    => intval($totalData),
					"recordsFiltered" => intval($totalFiltered),
					"data"            => $data,
					"new_hash"        => $this->security->get_csrf_hash()
				);

				echo json_encode($json_data);
			}
			elseif($type == 'delete-berkas'){
				if($this->input->post('permanent') == 1){
					// delete permanent

					// get user google token
					$user = $this->M_system->get('t_users', ['user_id' => $this->input->cookie('id')])->row();

					if($user->gdrive_token){
						$client = new Google_Client();
						$client->setAccessToken($user->gdrive_token);

						// renew token if expired
						if($client->isAccessTokenExpired()){
							$client->setAuthConfigFile(FCPATH.'client_secret.json');
							$client->fetchAccessTokenWithRefreshToken($user->gdrive_refresh_token);
							$client->setAccessToken(json_encode($client->getAccessToken()));
							
							// update new token to database
							$this->M_system->update('t_users', ['user_id' => $this->input->cookie('id')], ['gdrive_token' => json_encode($client->getAccessToken())]);
						}

						$drive = new Google_Service_Drive($client);

						$file = $this->M_basic->get('t_files', ['file_id' => $this->input->post('id')])->row();

						// delete file permanently
						$deleted = $drive->files->delete($file->path);
					}

					$this->M_basic->remove('t_files', ['file_id' => $this->input->post('id')]);
					$message = "Hapus permanen berkas berhasil.";
				}
				elseif($this->input->post('permanent') == 2){
					// soft delete
					$this->M_basic->update('t_files', ['file_id' => $this->input->post('id')], ['is_delete' => 2]);
					$message = "Hapus berkas berhasil.";
				}

				echo json_encode(['new_hash' => $this->security->get_csrf_hash(), 'message' => $message]);
			}
		}
	}

	public function download($fileId = null){
		if($fileId){
			$user = $this->M_system->get('t_users', ['user_id' => $this->input->cookie('id')])->row();

			if($user->gdrive_token){
				$client = new Google_Client();
				$client->setAccessToken($user->gdrive_token);

				// renew token if expired
				if($client->isAccessTokenExpired()){
					$client->setAuthConfigFile(FCPATH.'client_secret.json');
					$client->fetchAccessTokenWithRefreshToken($user->gdrive_refresh_token);
					$client->setAccessToken(json_encode($client->getAccessToken()));
					
					// update new token to database
					$this->M_system->update('t_users', ['user_id' => $this->input->cookie('id')], ['gdrive_token' => json_encode($client->getAccessToken())]);
				}

				$drive = new Google_Service_Drive($client);
				
				// get download link
				$response = $drive->files->get($fileId, ['fields' => 'webContentLink']);

				redirect($response->webContentLink);
			}
		}
		else{
			show_404();
		}
	}
}
