<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'vendor/autoload.php';

class Welcome extends MY_Controller {

	public function __construct(){
		parent::__construct();

		if(!$this->authentication()) redirect('auth/login');

		$this->load->model('M_basic');
	}

	public function index()
	{
		/* DON'T DELETE THIS SCRIPT, IT WILL BE USEFUL FOR LISTING FILES INSIDE WBERKAS FOLDER
		 *
		 *
		 *	$user = $this->M_system->get('t_users', ['user_id' => $this->input->cookie('id')])->row();
 		 *	
		 *	if($user->gdrive_token){
		 *		$client = new Google_Client();
		 *		$client->setAccessToken($user->gdrive_token);
 		 *	
		 *		if($client->isAccessTokenExpired()){
		 *			$client->fetchAccessTokenWithRefreshToken($user->gdrive_refresh_token);
		 *			$client->setAccessToken(json_encode($client->getAccessToken()));
 		 *	
		 *			$this->M_system->update('t_users', ['user_id' => $this->input->cookie('id')], ['gdrive_token' =>  *	json_encode($client->getAccessToken())]);
		 *		}
 		 *	
		 *		$drive = new Google_Service_Drive($client);
		 *		$folder = $drive->files->listFiles([
		 *			'q' => "mimeType = 'application/vnd.google-apps.folder' and name = 'wBerkas'"
		 *		]);
		 *		$files = $drive->files->listFiles([
		 *			'q' => "'".$folder->files[0]->id."' in parents"
		 *		]);
		 *		echo "<pre>";
		 *		var_dump($files);
		 *	}
		 */

		$data = $this->prepare_data("Dashboard", "dashboard", "");

		$data['user'] = $this->M_system->get('t_users', ['user_id' => $this->input->cookie('id')])->row();
		$data['total_files'] = $this->M_basic->count('t_files');

		$this->load->view('static/header', $data);
		$this->load->view('static/menu');
		$this->load->view('dashboard');
		$this->load->view('static/footer');
	}
}
