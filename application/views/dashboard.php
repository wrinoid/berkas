		<div class="content-wrapper">
			<section class="content-header">
				<h1>
					Dashboard
					<small>Control panel</small>
				</h1>
			</section>

			<section class="content">
				<div class="row">
				<?php
				if($user->gdrive_token){
					$client = new Google_Client();
					$client->setAccessToken($user->gdrive_token);

					if($client->isAccessTokenExpired()){
						$client->setAuthConfigFile(FCPATH.'client_secret.json');
						$client->fetchAccessTokenWithRefreshToken($user->gdrive_refresh_token);
						$client->setAccessToken(json_encode($client->getAccessToken()));

						$this->M_system->update('t_users', ['user_id' => $this->input->cookie('id')], ['gdrive_token' => json_encode($client->getAccessToken())]);
					}

					$drive = new Google_Service_Drive($client);
					$user = $drive->about->get(['fields' => '*'])->getUser();
				}
				?>
					<div class="col-lg-12 col-xs-12">
						<div class="box box-widget widget-user-2">
							<div class="box-header with-border">
								<h3 class="box-title">Akun Google Drive</h3>
							</div>
							<div class="widget-user-header">
								<div class="widget-user-image">
									<img class="img-circle" src="<?= $user['photoLink'] ?>" alt="User Avatar">
								</div>
								<h3 class="widget-user-username"><?= $user['displayName'] ?></h3>
								<h5 class="widget-user-desc"><?= $user['emailAddress'] ?></h5>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-xs-12">
						<div class="small-box bg-primary">
							<div class="inner">
								<h3><?= $total_files ?></h3>
								<p>Berkas</p>
							</div>
							<div class="icon">
								<i class="fa fa-file-text"></i>
							</div>
							<a href="<?= site_url('file') ?>" class="small-box-footer">Lihat list <i class="fa fa-arrow-circle-right"></i></a>
						</div>
					</div>
				</div>
			</section>
		</div>