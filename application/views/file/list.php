		<div class="content-wrapper">
			<section class="content-header">
				<h1>
					List
					<small>Berkas</small>
				</h1>
			</section>

			<section class="content">
				<div class="row">
					<div class="col-md-12">
						<div class="nav-tabs-custom">
							<ul class="nav nav-tabs pull-right">
								<li><a href="#deleted" data-toggle="tab">Deleted</a></li>
								<li class="active"><a href="#active" data-toggle="tab">Active</a></li>
								<li class="pull-left header">List Berkas</li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="active">
									<table id="table-files" class="table table-striped table-bordered dt-responsive" width="100%">
										<thead>
											<tr>
												<th>Nama</th>
												<th>Nomor</th>
												<th>Tanggal Berkas</th>
												<th></th>
											</tr>
										</thead>
										<tbody>

										</tbody>
									</table>
								</div>
								<div class="tab-pane" id="deleted">
									<table id="table-files-deleted" class="table table-striped table-bordered dt-responsive" width="100%">
										<thead>
											<tr>
												<th>Nama</th>
												<th>Nomor</th>
												<th>Tanggal Berkas</th>
												<th></th>
											</tr>
										</thead>
										<tbody>

										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>

		<!-- Modal -->
		<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header bg-red">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
						<h4 class="modal-title">Konfirmasi Hapus Berkas</h4>
					</div>
					<div class="modal-body">
						Apakah anda yakin akan menghapus berkas ini?
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-danger konfirm-delete" data-id="" data-type="1">Hapus Permanen</button>
						<button type="button" class="btn btn-danger konfirm-delete" data-id="" data-type="2">Hapus</button>
					</div>
				</div>
			</div>
		</div>