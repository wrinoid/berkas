		<div class="content-wrapper">
			<section class="content-header">
				<h1>
					Tambah
					<small>Berkas</small>
				</h1>
			</section>

			<section class="content">
				<div class="row">
					<div class="col-md-12">
						<?php if($this->session->flashdata('success')) echo $this->session->flashdata('success'); ?>
						<?php if($this->session->flashdata('error')) echo $this->session->flashdata('error'); ?>
						<div class="box">
							<form class="form-horizontal" method="POST" enctype="multipart/form-data">
								<input type="hidden" name="<?= $this->security->get_csrf_token_name() ?>" value="<?= $this->security->get_csrf_hash() ?>" id="csrf-token-transaction">
								<div class="box-header with-border">
									<h3 class="box-title">Tambah Berkas</h3>
								</div>
								<div class="box-body">
									<div class="form-group<?= (form_error('name') != "") ? " has-error" : "" ?>">
										<label class="col-sm-2 control-label">Nama</label>
										<div class="col-sm-4">
											<input type="text" name="name" class="form-control" value="<?=set_value('name')?>">
											<?= (form_error('name') != "") ? "<span class='help-block'>".form_error('name')."</span>" : "" ?>
										</div>
									</div>
									<div class="form-group<?= (form_error('number') != "") ? " has-error" : "" ?>">
										<label class="col-sm-2 control-label">Nomor</label>
										<div class="col-sm-4">
											<input type="text" name="number" class="form-control" value="<?=set_value('number')?>">
											<?= (form_error('number') != "") ? "<span class='help-block'>".form_error('number')."</span>" : "" ?>
										</div>
									</div>
									<div class="form-group<?= (form_error('date') != "") ? " has-error" : "" ?>">
										<label class="col-sm-2 control-label">Tanggal</label>
										<div class="col-sm-4">
											<input type="text" name="date" class="form-control" value="<?=set_value('date')?>">
											<?= (form_error('date') != "") ? "<span class='help-block'>".form_error('date')."</span>" : "" ?>
										</div>
									</div>
									<hr/>
									<div class="form-group<?= (form_error('hardfile') != "") ? " has-error" : "" ?>">
										<label class="col-sm-2 control-label">Fisik</label>
										<div class="col-sm-4">
											<div class="radio">
												<label>
													<input type="radio" name="hardfile" value="Ada"<?= (set_value('hardfile') == 'Ada') ? ' checked' : '' ?>> Ada
												</label>
											</div>
											<div class="radio">
												<label>
													<input type="radio" name="hardfile" value="Tidak Ada"<?= (set_value('hardfile', 'Tidak Ada') == 'Tidak Ada') ? ' checked' : '' ?>> Tidak Ada
												</label>
											</div>
											<?= (form_error('hardfile') != "") ? "<span class='help-block'>".form_error('hardfile')."</span>" : "" ?>
										</div>
									</div>
									<div class="form-group<?= (form_error('importance_level') != "") ? " has-error" : "" ?>">
										<label class="col-sm-2 control-label">Tingkat</label>
										<div class="col-sm-4">
											<div class="radio">
												<label>
													<input type="radio" name="importance_level" value="Penting"<?= (set_value('importance_level') == 'Penting') ? ' checked' : '' ?>> Penting
												</label>
											</div>
											<div class="radio">
												<label>
													<input type="radio" name="importance_level" value="Normal"<?= (set_value('importance_level', 'Normal') == 'Normal') ? ' checked' : '' ?>> Normal
												</label>
											</div>
											<?= (form_error('importance_level') != "") ? "<span class='help-block'>".form_error('importance_level')."</span>" : "" ?>
										</div>
									</div>
									<div class="form-group<?= (form_error('acceptance_date') != "") ? " has-error" : "" ?>">
										<label class="col-sm-2 control-label">Tanggal Terima</label>
										<div class="col-sm-4">
											<input type="text" name="acceptance_date" class="form-control" value="<?=set_value('acceptance_date')?>">
											<?= (form_error('acceptance_date') != "") ? "<span class='help-block'>".form_error('acceptance_date')."</span>" : "" ?>
										</div>
									</div>
									<div class="form-group<?= ($this->session->flashdata('file_error')) ? " has-error" : "" ?>">
										<label class="col-sm-2 control-label">File</label>
										<div class="col-sm-4">
											<input type="file" name="file" id="file" class="inputfile" />
											<label for="file"><span>Pilih berkas&hellip;</span></label>
											<?php
												if($this->session->flashdata('file_error')){
													echo "<span class='help-block'><ul>";
													foreach($this->session->flashdata('file_error') as $error){
														echo "<li>".$error."</li>";
													}
													echo "</ul></span>";
												}
											?>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Tags</label>
										<div class="col-sm-4">
											<select class="form-control" name="tags[]" multiple>
												<?php foreach($tags as $tag){ ?>
													<option value="<?= $tag->name ?>"><?= $tag->name ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
								<div class="box-footer">
									<button type="submit" class="btn btn-primary pull-right"><i class="fa fa-check"></i> Simpan</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</section>
		</div>