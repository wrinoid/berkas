		<div class="content-wrapper">
			<section class="content-header">
				<h1>
					Edit
					<small>Berkas</small>
				</h1>
			</section>

			<section class="content">
				<div class="row">
					<div class="col-md-12">
						<?php if($this->session->flashdata('success')) echo $this->session->flashdata('success'); ?>
						<?php if($this->session->flashdata('error')) echo $this->session->flashdata('error'); ?>
						<?php $disabled = ($file->is_delete == 2) ? ' disabled' : ''; ?>
						<div class="box">
							<form class="form-horizontal" method="POST">
								<input type="hidden" name="<?= $this->security->get_csrf_token_name() ?>" value="<?= $this->security->get_csrf_hash() ?>" id="csrf-token-transaction">
								<div class="box-header with-border">
									<h3 class="box-title">Edit Berkas</h3>
								</div>
								<div class="box-body">
									<div class="form-group<?= (form_error('name') != "") ? " has-error" : "" ?>">
										<label class="col-sm-2 control-label">Nama</label>
										<div class="col-sm-4">
											<input type="text" name="name" class="form-control" value="<?= set_value('name', $file->name) ?>"<?= $disabled ?>>
											<?= (form_error('name') != "") ? "<span class='help-block'>".form_error('name')."</span>" : "" ?>
										</div>
									</div>
									<div class="form-group<?= (form_error('number') != "") ? " has-error" : "" ?>">
										<label class="col-sm-2 control-label">Nomor</label>
										<div class="col-sm-4">
											<input type="text" name="number" class="form-control" value="<?= set_value('number', $file->number) ?>"<?= $disabled ?>>
											<?= (form_error('number') != "") ? "<span class='help-block'>".form_error('number')."</span>" : "" ?>
										</div>
									</div>
									<div class="form-group<?= (form_error('date') != "") ? " has-error" : "" ?>">
										<label class="col-sm-2 control-label">Tanggal</label>
										<div class="col-sm-4">
											<input type="text" name="date" class="form-control" value="<?= set_value('date', $file->date) ?>"<?= $disabled ?>>
											<?= (form_error('date') != "") ? "<span class='help-block'>".form_error('date')."</span>" : "" ?>
										</div>
									</div>
									<hr/>
									<div class="form-group<?= (form_error('hardfile') != "") ? " has-error" : "" ?>">
										<label class="col-sm-2 control-label">Fisik</label>
										<div class="col-sm-4">
											<div class="radio">
												<label>
													<input type="radio" name="hardfile" value="Ada"<?= (set_value('hardfile', $file->hardfile) == 'Ada') ? ' checked' : '' ?><?= $disabled ?>> Ada
												</label>
											</div>
											<div class="radio">
												<label>
													<input type="radio" name="hardfile" value="Tidak Ada"<?= (set_value('hardfile', $file->hardfile) == 'Tidak Ada') ? ' checked' : '' ?><?= $disabled ?>> Tidak Ada
												</label>
											</div>
											<?= (form_error('hardfile') != "") ? "<span class='help-block'>".form_error('hardfile')."</span>" : "" ?>
										</div>
									</div>
									<div class="form-group<?= (form_error('importance_level') != "") ? " has-error" : "" ?>">
										<label class="col-sm-2 control-label">Tingkat</label>
										<div class="col-sm-4">
											<div class="radio">
												<label>
													<input type="radio" name="importance_level" value="Penting"<?= (set_value('importance_level', $file->importance_level) == 'Penting') ? ' checked' : '' ?><?= $disabled ?>> Penting
												</label>
											</div>
											<div class="radio">
												<label>
													<input type="radio" name="importance_level" value="Normal"<?= (set_value('importance_level', $file->importance_level) == 'Normal') ? ' checked' : '' ?><?= $disabled ?>> Normal
												</label>
											</div>
											<?= (form_error('importance_level') != "") ? "<span class='help-block'>".form_error('importance_level')."</span>" : "" ?>
										</div>
									</div>
									<div class="form-group<?= (form_error('acceptance_date') != "") ? " has-error" : "" ?>">
										<label class="col-sm-2 control-label">Tanggal Terima</label>
										<div class="col-sm-4">
											<input type="text" name="acceptance_date" class="form-control" value="<?= set_value('acceptance_date', $file->acceptance_date) ?>"<?= $disabled ?>>
											<?= (form_error('acceptance_date') != "") ? "<span class='help-block'>".form_error('acceptance_date')."</span>" : "" ?>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Tags</label>
										<div class="col-sm-4">
											<select class="form-control" name="tags[]" multiple<?= $disabled ?>>
												<?php $selected_tags = explode(',', $file->tags); ?>
												<?php foreach($tags as $tag){ ?>
													<option value="<?= $tag->name ?>"<?= (in_array($tag->name, $selected_tags)) ? ' selected' : '' ?>><?= $tag->name ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
								<div class="box-footer">
									<button type="submit" class="btn btn-primary pull-right"<?= $disabled ?>><i class="fa fa-check"></i> Simpan</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</section>
		</div>