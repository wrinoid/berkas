		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				Load time : {elapsed_time}s &nbsp;&nbsp;&nbsp;
				<b>Version</b> 1.3.0
			</div>
			<strong>Copyright &copy; 2018 <a href="https://wrino.id">wrino.id</a>.</strong> All rights reserved.
		</footer>
		<input type="hidden" id="csrf" class="csrf" name="csrf_test_name" value="<?= $this->security->get_csrf_hash() ?>">
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 2.2.3 -->
	<script src="<?= base_url('assets/plugins/jQuery/jquery-2.2.3.min.js') ?>"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
		$.widget.bridge('uibutton', $.ui.button);
	</script>
	<!-- Bootstrap 3.3.6 -->
	<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
	<!-- AdminLTE App -->
	<script src="<?= base_url('assets/js/app.min.js') ?>"></script>
	<!-- toastr -->
	<script src="<?= base_url('assets/plugins/toastr/toastr.min.js') ?>"></script>
	<!-- daterangepicker -->
	<script src="<?= base_url('assets/plugins/daterangepicker/moment.js') ?>"></script>
	<script src="<?= base_url('assets/plugins/daterangepicker/daterangepicker.js') ?>"></script>
	<!-- Custom -->
	<script src="<?= base_url('assets/js/jquery.custom-file-input.js') ?>"></script>
	<script>
		var site_url = '<?= site_url() ?>';
	</script>
	<?= $script_bottom ?>
</body>
</html>
