		<aside class="main-sidebar">
			<section class="sidebar">
				<ul class="sidebar-menu">
					<li class="header">MAIN NAVIGATION</li>
					<li class="<?= $menu == 'dashboard' ? 'active' : '' ?>">
						<a href="<?= site_url('welcome') ?>">
							<i class="fa fa-dashboard"></i> <span>Dashboard</span>
						</a>
					</li>
					<li class="treeview<?= $menu == 'berkas' ? ' active' : '' ?>">
						<a href="#">
							<i class="fa fa-file-text"></i> <span>Berkas</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li<?= $anak_menu == 'list' ? ' class="active"' : '' ?>><a href="<?= site_url('file') ?>"> List</a></li>
							<li<?= $anak_menu == 'add' ? ' class="active"' : '' ?>><a href="<?= site_url('file/add') ?>"> Tambah</a></li>
						</ul>
					</li>
				</ul>
			</section>
		</aside>