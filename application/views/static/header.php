<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?= $title ?></title>
	<link rel="icon" href="<?=base_url('assets/img/favicon.png')?>" type="image/png"/>
	<link rel="shortcut icon" href="<?=base_url('assets/img/favicon.png')?>" type="image/png"/>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<?= $script_top ?>
	<!-- Theme style -->
	<link rel="stylesheet" href="<?= base_url('assets/css/AdminLTE.min.css') ?>">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
	folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="<?= base_url('assets/css/skins/_all-skins.min.css') ?>">
	<!-- toastr -->
	<link rel="stylesheet" href="<?= base_url('assets/plugins/toastr/toastr.min.css') ?>">
	<!-- daterangepicker -->
	<link rel="stylesheet" href="<?= base_url('assets/plugins/daterangepicker/daterangepicker.css') ?>">
	<!-- Custom -->
	<link rel="stylesheet" href="<?= base_url('assets/css/custom.css') ?>">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="hold-transition skin-black sidebar-mini">
	<div class="wrapper">
		<header class="main-header">
			<a href="<?= site_url() ?>" class="logo">
				<span class="logo-mini"><b>w</b>B</span>
				<span class="logo-lg"><b>w</b>Berkas</span>
			</a>
			<nav class="navbar navbar-static-top">
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="<?= base_url('assets/img/user-160x160.jpg') ?>" class="user-image" alt="User Image">
								<span class="hidden-xs"><?= $this->input->cookie('name') ?></span>
							</a>
							<ul class="dropdown-menu">
								<li class="user-header">
									<img src="<?= base_url('assets/img/user-160x160.jpg') ?>" class="img-circle" alt="User Image">
									<p>
										<?= $this->input->cookie('name') ?>
										<small><?= date_diff(date_create('1994-01-08'), date_create('now'))->y ?> Years old</small>
									</p>
								</li>
								<li class="user-footer">
									<div class="pull-right">
										<a href="<?= site_url('auth/logout') ?>" class="btn btn-danger btn-flat"><i class="fa fa-sign-out"></i> Logout</a>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
		</header>