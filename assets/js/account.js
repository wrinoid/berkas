// datatable
var t = $('.datatable').DataTable({
	"columnDefs": [{
		"searchable": false,
		"orderable": false,
		"targets": 0
	},
	{
		"searchable": false,
		"orderable": false,
		"targets": -1
	}],
	"order": [[ 1, 'asc' ]]
});

t.on('order.dt search.dt', function(){
	t.column(0, {search:'applied', order:'applied'}).nodes().each(function(cell, i){
		cell.innerHTML = i+1;
	});
}).draw();

// autonumeric
$('#account-balance').autoNumeric('init');

// form validation
$('#form-add-account').parsley().on('field:validated', function() {
	if($('.parsley-error').length !== 0){
		$('.parsley-error').closest('.form-group').addClass('has-error');
	}

	if($('.parsley-success').length !== 0){
		$('.parsley-success').closest('.form-group').removeClass('has-error');
		$('.parsley-success').next().html('');
	}
});

// delete account
$('.delete-account-btn').click(function(){
	var id = $(this).data('id');
	$('#delete-account-modal').find('.modal-footer a').attr('href', site_url + 'account/delete?id=' + id);
	$('#delete-account-modal').modal('show');
});